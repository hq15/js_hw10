const passwordSet = document.querySelector(".js-password");
const passwordSubmit = document.querySelector(".js-sub-password");
const formEvent = document.querySelector(".password-form");
const warningText = document.querySelector(".warning");

formEvent.addEventListener("click", (event) => {
  warningText.innerHTML = "";
  if (event.target.closest(".js-password ~ .fas")) {
    showPassword('js-pass-visible', passwordSet, 'js-pass-hidden', event);
  }

  if (event.target.closest(".js-sub-password ~ .fas")) {
    showPassword('js-sub-pass-visible', passwordSubmit, 'js-sub-pass-hidden', event);
  }

  if (event.target.classList.contains("btn")) {
    submitPassword(warningText, passwordSet, passwordSubmit, event);
  }
});

// password display switching section
function showPassword(classVisible, passInput, classHidden, event){
  if (event.target.classList.contains('js-click-active')) {
      
    document
      .querySelector(`.${classVisible}`)
      .classList.toggle('js-click-active');
    document
      .querySelector(`.${classHidden}`)
      .classList.toggle('js-click-active');
    if (passInput.type === "password") {
      passInput.type = "text";
    } else {
      passInput.type = "password";
    };
  };
};

// section for checking the equality of password data and password confirmation data
function submitPassword(element, password, passwordСonfirm, event) {
  element.innerHTML = "";
  event.preventDefault();
  if (password.value !== passwordСonfirm.value || password.value === '') {
    element.innerHTML = "Нужно ввести одинаковые значения";
    element.style.color = "red";
  } else {
    alert("You are welcome");
    password.value = "";
    passwordСonfirm.value = "";
  };
};
